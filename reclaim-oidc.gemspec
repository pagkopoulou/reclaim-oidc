Gem::Specification.new do |s|
  s.name        = 'reclaim-oidc'
  s.version     = '0.0.5'
  s.date        = '2019-04-30'
  s.summary     = "re:claimID OpenID Connect CLI"
  s.description = "Used to manage re:claimID OpenID Connect clients and OpenID Connect Provider configuration(s)"
  s.authors     = ["Martin Schanzenbach"]
  s.email       = 'mschanzenbach@posteo.de'
  s.files       = ["lib/reclaim_oidc.rb"]
  s.executables << "reclaim-oidc"
  s.homepage    =
    'https://gitlab.com/reclaimid/reclaim-oidc'
  s.license       = 'AGPL-3.0'
end
