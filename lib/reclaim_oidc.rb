require 'net/http'
require 'json'

$VERSION_MAJOR = 0
$VERSION_MINOR = 0
$VERSION_MICRO = 5

class ReclaimOidc
  def initialize(verbose=false, url='http://localhost:7776')
    @verbose = verbose
    @url = url
    @client_secret = get_client_secret()
  end
  def self.version
    return "#{$VERSION_MAJOR}.#{$VERSION_MINOR}.#{$VERSION_MICRO}"
  end
  def parse_identities_from_http(body)
    arr = JSON.parse(body)
    ids = []
    arr.each do |obj|
      obj["secret"] = @client_secret
      ids << ReclaimOidc::Client.from_json(obj)
    end
    ids
  end

  def get_client_secret
    uri = URI(@url + '/config/reclaim-rest-plugin')
    begin
      resp = JSON.parse Net::HTTP.get(uri)
      return resp["OIDC_CLIENT_SECRET"]
    rescue Errno::ECONNREFUSED => e
      puts "ERROR: REST service is not running"
      exit
    end
  end

  def get_clients
    uri = URI(@url + '/identity/all')
    ids = parse_identities_from_http(Net::HTTP.get(uri))
    result = []
    ids.each do |id|
      uri = URI(@url + "/namestore/#{id.name}")
      id.parse_client_info(JSON.parse(Net::HTTP.get(uri)))
      next if id.redirect_uri.nil?
      result << id
    end
    result
  end
  def add_client(name,redirect_uri,description)
    raise if redirect_uri.nil? or description.nil? or name.nil?
    uri = URI(@url + '/identity')
    payload = {'name' => "#{name}"}
    #resp = Net::HTTP.post(uri, payload)
    #req = Net::HTTP::Post.new(uri, payload.to_json)
    Net::HTTP.start(uri.host, uri.port) do |http|
      resp = http.post(uri.path, payload.to_json)
    end
    uri = URI(@url + "/namestore/#{name}")
    records = {'record_name' => "@",
               'data' => []}
    records["data"] << {'record_type' => "RECLAIM_OIDC_CLIENT",
              'value' => description,
              'expiration_time' => "1h",
              'flag' => 8}
    records["data"] << {'record_type' => "RECLAIM_OIDC_REDIRECT",
              'value' => redirect_uri,
              'expiration_time' => "1h",
              'flag' => 8}
    Net::HTTP.start(uri.host, uri.port) do |http|
      resp = http.post(uri.path,records.to_json)
    end
  end
  def delete_client(name)
    raise if name.nil?
    uri = URI(@url + "/identity/name/#{name}")
    Net::HTTP.start(uri.host, uri.port) do |http|
      request = Net::HTTP::Delete.new uri
      resp = http.request request # Net::HTTPResponse object
    end
  end
  def get_op_info
    uri = URI(@url + '/config/reclaim-rest-plugin')
    resp = JSON.parse Net::HTTP.get(uri)
    op = {}
    op['jwt_key'] = resp["JWT_SECRET"]
    op['jwt_algo'] = 'HS512' # FIXME
    host = 'http://localhost:7776'
    op['authz_endpoint'] = host + '/openid/authorize'
    op['token_endpoint'] = host + '/openid/token'
    op['userinfo_endpoint'] = host + '/openid/userinfo'
    op
  end  
  def set_jwt_secret(jwt_secret)
    uri = URI(@url + '/config/reclaim-rest-plugin')
    request = Net::HTTP::Post.new(uri, 'Content-Type' => 'application/json')
    request.body = {"JWT_SECRET": jwt_secret}.to_json
    resp = Net::HTTP.start(uri.host, uri.port) do |http|
      http.request request
    end
  end

  class Client
    attr_reader      :name, :key, :description, :redirect_uri, :secret
    def initialize(name, key, secret)
      @name = name
      @key = key
      @secret = secret
    end
    def self.from_json(obj)
      id = Client.new(obj['name'], obj['pubkey'], obj['secret'])
    end
    def parse_client_info(obj)
      obj.each do |record|
        if "@" != record["record_name"]
          next
        end
        record["data"].each do |data|
          if (data['record_type'] == 'RECLAIM_OIDC_CLIENT')
            @description = data['value']
          end
          if (data['record_type'] == 'RECLAIM_OIDC_REDIRECT')
            @redirect_uri = data['value']
          end
        end
      end
    end
  end
end
